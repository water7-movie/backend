package vn.movie.service;

import vn.movie.entity.User;
import vn.movie.model.request.AbstractUserRequest;

import java.util.List;

public interface UserService {
    User createUser(AbstractUserRequest userRequest);
    User findByEmail(String email);
    List<User> findByEmailAndStatus(String email, int status);
}
