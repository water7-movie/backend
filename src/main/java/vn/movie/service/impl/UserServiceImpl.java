package vn.movie.service.impl;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import vn.movie.entity.ERole;
import vn.movie.entity.Role;
import vn.movie.entity.User;
import vn.movie.exception.ServiceException;
import vn.movie.model.request.AbstractUserRequest;
import vn.movie.repository.RoleRepository;
import vn.movie.repository.UserRepository;
import vn.movie.service.UserService;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public User createUser(AbstractUserRequest userRequest) {
        User localUser = userRepository.findByEmail(userRequest.getEmail());
        if(localUser != null) {
            throw new ServiceException("Email " + localUser.getEmail() + " already exists. Nothing will be done!");
        } else {
            localUser = new User(userRequest.getEmail(),
                    new BCryptPasswordEncoder().encode(userRequest.getPassword()),
                    userRequest.getFullName(),
                    userRequest.getPhone(),
                    userRequest.getAddress(),
                    1,
                    userRequest.getDateOfBirth(),
                    userRequest.getGender());
            Set<String> strRoles = userRequest.getRoles();
            Set<Role> roles = new HashSet<>();

            if (strRoles == null) {
                Role userRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
            } else {
                strRoles.forEach(role -> {
                    switch (role) {
                        case "ROLE_ADMIN":
                            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(adminRole);
                            break;
                        case "ROLE_MODERATOR":
                            Role modRole = roleRepository.findByName(ERole.ROLE_MODERATOR)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(modRole);
                            break;
                        case "ROLE_CUSTOMER":
                            Role cusRole = roleRepository.findByName(ERole.ROLE_CUSTOMER)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(cusRole);
                            break;
                    }
                });
            }

            localUser.setRoles(roles);

        }
        return userRepository.save(localUser);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<User> findByEmailAndStatus(String email, int status) {
        return userRepository.findByEmailAndStatus(email, status)
                .map(Collections::singletonList)
                .orElseGet(Collections::emptyList);
    }
}
