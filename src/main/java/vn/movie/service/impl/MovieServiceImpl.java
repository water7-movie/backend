package vn.movie.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import vn.movie.entity.Movie;
import vn.movie.exception.file.FileStorageException;
import vn.movie.exception.movie.NotFoundException;
import vn.movie.exception.movie.RequestException;
import vn.movie.model.request.MovieRequest;
import vn.movie.repository.MovieRepository;
import vn.movie.repository.ShowingScheduleRepository;
import vn.movie.service.FileStorageService;
import vn.movie.service.MovieService;
import vn.movie.util.FileStorageProperties;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository movieRepository;
    private final ShowingScheduleRepository showingScheduleRepository;
    private final Path fileStorageLocation;

    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository,
                            ShowingScheduleRepository showingScheduleRepository,
                            FileStorageProperties fileStorageProperties) {
        this.movieRepository = movieRepository;
        this.showingScheduleRepository = showingScheduleRepository;
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    @Autowired
    private FileStorageService fileStorageService;

    @Override
    public Page<Movie> listAllShowingMoviesWithPagination(Pageable pageable) {
        Page<Movie> moviePage = movieRepository.findShowingMoviesWithQuery(pageable);
//        for (Movie item : moviePage.getContent()) {
//            setThumbnailMovie(item);
//        }
        return moviePage;
    }

    @Override
    public Page<Movie> listAllComingMoviesWithPagination(Pageable pageable) {
        Page<Movie> moviePage = movieRepository.findComingMoviesWithQuery(pageable);
//        for (Movie item : moviePage.getContent()) {
//            setThumbnailMovie(item);
//        }
        return moviePage;
    }

    @Override
    public Page<Movie> getMovieList(Pageable pageable) {
        Page<Movie> moviePage = movieRepository.findMovieNDelete(pageable);
//        for (Movie item : moviePage.getContent()) {
//            setThumbnailMovie(item);
//        }
        return moviePage;
    }

    @Override
    public Movie createMovie(MovieRequest movieRequest) {
        Movie localMovie = movieRepository.findByTitle(movieRequest.getTitle());
        if (localMovie != null) {
            Map<String, String> errors = new HashMap<>();
            errors.put("title", "Title already exists!");
            throw new RequestException(
                    "The given data was invalid.",
                    errors,
                    HttpStatus.CONFLICT
            );
        } else {
            localMovie = convertRequestToObject(new Integer(0).longValue(), movieRequest);

            if (movieRequest.getImageFile() != null) {
                // set thumbnail field
                // Normalize thumbnail
                MultipartFile[] files = movieRequest.getImageFile();
                int index = 0;
                for (MultipartFile file : files) {
                    if (index == 0) {
                        String oldFileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
                        String extension = oldFileName.substring(oldFileName.lastIndexOf("."));
                        String newFileName = StringUtils.cleanPath(localMovie.getTitle()).toLowerCase().replaceAll(" ", "-");
                        String newFileNameFull = newFileName + extension;
                    }
                    index++;
                }
                localMovie = movieRepository.save(localMovie);
                fileStorageService.storeListFile("movie", localMovie.getId(), localMovie.getTitle(), movieRequest.getImageFile());
            } else {
                localMovie = movieRepository.save(localMovie);
            }
        }
        return localMovie;
    }

    @Override
    public Movie editMovie(MovieRequest movieRequest, Long id) {
        Optional<Movie> optionalMovie = movieRepository.findById(id);
        Movie movieByTitle = movieRepository.findByTitle(movieRequest.getTitle());

        if (!optionalMovie.isPresent()) {
            throw new NotFoundException("Movie not found for id {" + id + "}");
        } else {
            if (movieByTitle != null) {
                if (!optionalMovie.get().getTitle().equals(movieByTitle.getTitle())) {
                    Map<String, String> errors = new HashMap<>();
                    errors.put("title", "Title already exists!");
                    throw new RequestException(
                            "The given data was invalid.",
                            errors,
                            HttpStatus.CONFLICT
                    );
                }
            }

            Movie localMovie = convertRequestToObject(id, movieRequest);
            if (movieRequest.getImageFile() != null) {
                // set thumbnail field
                // Normalize thumbnail
                MultipartFile[] files = movieRequest.getImageFile();
                int index = 0;
                for (MultipartFile file : files) {
                    if (index == 0) {
                        String oldFileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
                        String extension = oldFileName.substring(oldFileName.lastIndexOf("."));
                        String newFileName = StringUtils.cleanPath(localMovie.getTitle()).toLowerCase().replaceAll(" ", "-");
                        String newFileNameFull = newFileName + extension;
                    }
                    index++;
                }
                localMovie = movieRepository.save(localMovie);
//                fileStorageService.storeListFile("movie", localMovie.getId(), localMovie.getTitle(), movieRequest.getImageFile());
            } else {
                localMovie = movieRepository.save(localMovie);
            }
            return localMovie;
        }
    }

    @Override
    public Movie getMovieById(Long id) {
        Movie item = movieRepository.findById(id).get();
//        setThumbnailMovie(item);
        return item;
    }

    @Override
    public int setStatusForMovies(Long id, int status) {
        // If status not 0, 1, -1 throw 422
        switch (status) {
            case 0:
            case 1:
            case -1: {
                // check id movie is existed, throw 404 if not
                if (movieRepository.findById(id).isPresent()) {
                    return movieRepository.setStatusForMovie(status, id);
                } else {
                    Map<String, String> errors = new HashMap<>();
                    errors.put("id", "Movie was not found for the id {" + id + "}");
                    throw new RequestException(
                            "The given data was invalid.",
                            errors,
                            HttpStatus.NOT_FOUND
                    );
                }
            }
            default: {
                Map<String, String> errors = new HashMap<>();
                errors.put("status", "Status is not exits.");
                throw new RequestException(
                        "The given data was invalid.",
                        errors,
                        HttpStatus.UNPROCESSABLE_ENTITY
                );
            }
        }
    }

    @Override
    public List<Movie> getListMovieByDate(String date) {
        return showingScheduleRepository.findMoviesByDate(date);
    }

    private Movie convertRequestToObject(Long id, MovieRequest movieRequest) {
        return new Movie(id, movieRequest.getTitle(),
                movieRequest.getDirector(), movieRequest.getActors(), movieRequest.getGenre(),
                movieRequest.getReleaseDate(), movieRequest.getDuration(),
                movieRequest.getDescription(), movieRequest.getStatus());
    }

//    private void setThumbnailMovie(Movie item) {
//        String fileDownloadUri = null;
//        if (item.getThumbnail() != null) {
//            fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//                    .path("/image/movie/")
//                    .path("/" + item.getId() + "/")
//                    .path("/" + item.getThumbnail())
//                    .toUriString();
//        }
//        item.setThumbnail(fileDownloadUri);
//    }
}
