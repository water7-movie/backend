package vn.movie.service.impl;

import org.springframework.stereotype.Service;
import vn.movie.entity.Booking;
import vn.movie.entity.Seat;
import vn.movie.entity.ShowingSchedule;
import vn.movie.exception.ServiceException;
import vn.movie.model.request.BookingRequest;
import vn.movie.model.request.TicketInfoRequest;
import vn.movie.model.response.success.SeatResponse;
import vn.movie.repository.*;
import vn.movie.service.BookingService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final UserRepository userRepository;
    private final MovieRepository movieRepository;
    private final SeatRepository seatRepository;
    private final ShowingScheduleRepository showingScheduleRepository;

    public BookingServiceImpl(BookingRepository bookingRepository,
                              UserRepository userRepository,
                              MovieRepository movieRepository,
                              SeatRepository seatRepository,
                              ShowingScheduleRepository showingScheduleRepository) {
        this.bookingRepository = bookingRepository;
        this.userRepository = userRepository;
        this.movieRepository = movieRepository;
        this.seatRepository = seatRepository;
        this.showingScheduleRepository = showingScheduleRepository;
    }


    @Override
    public List<Booking> saveBooking(BookingRequest bookingRequest) throws ParseException {
        List<Long> listSeat = bookingRequest.getSeatIds();
        List<Booking> bookings = new ArrayList<>();

        for(Long seatId: listSeat){
            Booking localBooking = bookingRepository
                    .getBookingByShowingScheduleAndSeat(bookingRequest.getShowingScheduleId(), seatId);
            if(localBooking != null) {
                throw new ServiceException("Seat " + localBooking.getSeat().getRow()
                        + localBooking.getSeat().getNumber() + " already booked! Please choose other");
            } else {
                localBooking = new Booking();
                localBooking.setBookingDate(new SimpleDateFormat("yyyy-MM-dd").parse(bookingRequest.getBookingDate()));
                localBooking.setGrandTotal(bookingRequest.getGrandTotal());
                localBooking.setUser(userRepository.findById(bookingRequest.getCustomerId()).get());
                localBooking.setMovie(movieRepository.findById(bookingRequest.getMovieId()).get());
                localBooking.setSeat(seatRepository.findById(seatId).get());
                localBooking.setShowingSchedule(showingScheduleRepository.findById(bookingRequest.getShowingScheduleId()).get());
            }
            bookings.add(localBooking);
        }
        List<Booking> results = bookingRepository.saveAll(bookings);

        return results;
    }

    @Override
    public SeatResponse getAllSeatsByMoviesAndDate(String showingDate, Long movieId) {
        List<Seat> seats = bookingRepository.getAllSeatsByMoviesAndShowingDate(showingDate, movieId);
        ShowingSchedule showingSchedule = showingScheduleRepository.findShowingSchedulesByDateAndMovie(showingDate,
                movieId);
        TicketInfoRequest ticketInfo = bookingRepository.getTicketInfo(showingSchedule.getId());

        SeatResponse seatsCustom = new SeatResponse(seats, showingSchedule.getId(), ticketInfo);
        return seatsCustom;
    }
}
