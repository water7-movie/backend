package vn.movie.service.impl;

import org.springframework.stereotype.Service;
import vn.movie.entity.ShowingSchedule;
import vn.movie.repository.ShowingScheduleRepository;
import vn.movie.service.ShowingScheduleService;

import java.util.List;

@Service
public class ShowingScheduleServiceImpl implements ShowingScheduleService {

    private final ShowingScheduleRepository showingScheduleRepository;

    public ShowingScheduleServiceImpl(ShowingScheduleRepository showingScheduleRepository) {
        this.showingScheduleRepository = showingScheduleRepository;
    }

    @Override
    public List<ShowingSchedule> listMoviesByDate(String date, Long movieId) {
        return showingScheduleRepository.findByDate(date, movieId);
    }


}
