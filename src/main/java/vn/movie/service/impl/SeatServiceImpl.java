package vn.movie.service.impl;

import org.springframework.stereotype.Service;
import vn.movie.entity.Seat;
import vn.movie.repository.BookingRepository;
import vn.movie.service.SeatService;

import java.util.List;

@Service
public class SeatServiceImpl implements SeatService {

    private final BookingRepository bookingRepository;

    public SeatServiceImpl(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    @Override
    public List<Seat> listBookedSeatsInSchedule(Long showingScheduleId, Long movieId) {
        return bookingRepository.findBookedSeats(showingScheduleId, movieId);
    }
}
