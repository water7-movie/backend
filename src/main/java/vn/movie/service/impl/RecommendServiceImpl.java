package vn.movie.service.impl;

import org.springframework.stereotype.Service;
import vn.movie.model.mining.Transaction;
import vn.movie.model.mining.Tuple;
import vn.movie.repository.BookingRepository;
import vn.movie.repository.MovieRepository;
import vn.movie.repository.UserRepository;
import vn.movie.service.RecommendService;

import java.util.*;

@Service
public class RecommendServiceImpl implements RecommendService {
    static Set<Tuple> c;
    static Set<Tuple> l;
    static long d[][];
    private final long minSupportCount;
    static double minConfidence = 0.6;

    private final MovieRepository movieRepository;
    private final UserRepository userRepository;
    private final BookingRepository bookingRepository;

    public RecommendServiceImpl(MovieRepository movieRepository,
                                UserRepository userRepository,
                                BookingRepository bookingRepository) {
        this.movieRepository = movieRepository;
        this.userRepository = userRepository;
        this.bookingRepository = bookingRepository;

        this.minSupportCount = Math.round(0.2 * userRepository.count());
    }

    @Override
    public Set<Long> recommendMovie(Long movieId) {
        getDB();

        c = new HashSet<>();
        l = new HashSet<>();

        int i, j, m, n;
        Set<Long> candidate_set = new HashSet<>();
        for (i = 0; i < d.length; i++) {
            System.out.println("Transaction Number: " + (i + 1) + ":");
            for (j = 0; j < d[i].length; j++) {
                System.out.print("Item number " + (j + 1) + " = ");
                System.out.println(d[i][j]);
                candidate_set.add(d[i][j]);
            }
        }

        for (Long aLong : candidate_set) {
            Set<Long> s = new HashSet<>();
            s.add(aLong);
            Tuple t = new Tuple(s, count(s));
            c.add(t);
        }

        prune();

        return generateFrequentItemsets(movieId);

    }

    /*
        conf(X => Y) = sup_count(X U Y) / sup_count(X)
    */
    public Set<Long> getAssociationRules(long x) {
        Set<Long> finalRecommendItems = new HashSet<>();
        for (Tuple t : l) {
            if (t.getItemset().contains(x) && t.getItemset().size() == 2) {
                Set<Long> temp = new HashSet<>();
                temp.add(x);
                int sup_count = count(temp);
                double conf = (double) t.getSupport() / sup_count;
                System.out.println(conf);

                if(conf >= minConfidence) {
                    for(long lf : t.getItemset()) {
                        if(lf != x) finalRecommendItems.add(lf);
                    }
                }
            }
        }

        return finalRecommendItems;
    }

    private Set<Long> generateFrequentItemsets(Long movieId) {
        boolean toBeContinued = true;
        long element = 0;
        int size = 1;
        Set<Set> candidate_set = new HashSet<>();
        while (toBeContinued) {
            candidate_set.clear();
            c.clear();
            Iterator<Tuple> iterator = l.iterator();
            while (iterator.hasNext()) {
                Tuple t1 = iterator.next();
                Set<Long> temp = t1.getItemset();
                Iterator<Tuple> it2 = l.iterator();
                while (it2.hasNext()) {
                    Tuple t2 = it2.next();
                    Iterator<Long> it3 = t2.getItemset().iterator();
                    while (it3.hasNext()) {
                        try {
                            element = it3.next();
                        } catch (ConcurrentModificationException e) {
                            // Sometimes this Exception gets thrown, so simply break in that case.
                            break;
                        }
                        temp.add(element);
                        if (temp.size() != size) {
                            Long[] int_arr = temp.toArray(new Long[0]);
                            Set<Long> temp2 = new HashSet<>();
                            for (Long x : int_arr) {
                                temp2.add(x);
                            }
                            candidate_set.add(temp2);
                            temp.remove(element);
                        }
                    }
                }
            }
            Iterator<Set> candidate_set_iterator = candidate_set.iterator();
            while (candidate_set_iterator.hasNext()) {
                Set s = candidate_set_iterator.next();
                // These lines cause warnings, as the candidate_set Set stores a raw set.
                c.add(new Tuple(s, count(s)));
            }
            prune();
            if (l.size() <= 1) {
                toBeContinued = false;
            }
            size++;
        }
//        System.out.println("\n=+= FINAL LIST =+=");
//        for (Tuple t : l) {
//            System.out.println(t.getItemset() + " : " + t.getSupport());
//        }

        return getAssociationRules(movieId);

    }

    private void prune() {
        l.clear();
        Iterator<Tuple> iterator = c.iterator();
        while (iterator.hasNext()) {
            Tuple t = iterator.next();
            if (t.getSupport() >= minSupportCount) {
                l.add(t);
            }
        }
        System.out.println("-+- L -+-");
        for (Tuple t : l) {
            System.out.println(t.getItemset() + " : " + t.getSupport());
        }
    }

    private int count(Set<Long> s) {
        int i, j, k;
        int support = 0;
        int count;
        boolean containsElement;
        for (i = 0; i < d.length; i++) {
            count = 0;
            for (long element : s) {
                containsElement = false;
                for (k = 0; k < d[i].length; k++) {
                    if (element == d[i][k]) {
                        containsElement = true;
                        count++;
                        break;
                    }
                }
                if (!containsElement) {
                    break;
                }
            }
            if (count == s.size()) {
                support++;
            }
        }
        return support;
    }

    private void getDB() {
        List<Transaction> transactionList = bookingRepository.listAllTransaction();
        Map<Long, List<Long>> m = new HashMap<>();
        List<Long> temp;

        for (Transaction t : transactionList) {
            temp = m.get(t.getCustomerId());

            if (temp == null) temp = new LinkedList<>();

            temp.add(t.getMovieId());
            m.put(t.getCustomerId(), temp);
        }

        Set<Long> keyset = m.keySet();
        d = new long[keyset.size()][];
        Iterator<Long> iterator = keyset.iterator();
        int count = 0;
        while (iterator.hasNext()) {
            temp = m.get(iterator.next());
            Long[] int_arr = temp.toArray(new Long[0]);
            d[count] = new long[int_arr.length];
            for (int i = 0; i < d[count].length; i++) {
                d[count][i] = int_arr[i].intValue();
            }
            count++;
        }
    }

}
