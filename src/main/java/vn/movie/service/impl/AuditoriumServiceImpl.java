package vn.movie.service.impl;

import org.springframework.stereotype.Service;
import vn.movie.entity.Auditorium;
import vn.movie.repository.AuditoriumRepository;
import vn.movie.repository.ShowingScheduleRepository;
import vn.movie.service.AuditoriumService;

import java.util.List;

@Service
public class AuditoriumServiceImpl implements AuditoriumService {

    private final AuditoriumRepository auditoriumRepository;
    private final ShowingScheduleRepository showingScheduleRepository;

    public AuditoriumServiceImpl(AuditoriumRepository auditoriumRepository,
                                 ShowingScheduleRepository showingScheduleRepository) {
        this.auditoriumRepository = auditoriumRepository;
        this.showingScheduleRepository = showingScheduleRepository;
    }

    @Override
    public Auditorium findAuditoriumByDateAndMovieId(String date, Long movieId) {
        return showingScheduleRepository.findAuditorium(date, movieId);
    }

    @Override
    public List<Auditorium> listAll() {
        return auditoriumRepository.findAll();
    }
}
