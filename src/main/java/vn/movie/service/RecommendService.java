package vn.movie.service;

import java.util.Set;

public interface RecommendService {
    Set<Long> recommendMovie(Long movieId);
}
