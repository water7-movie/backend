package vn.movie.service;

import vn.movie.entity.Seat;

import java.util.List;

public interface SeatService {

    List<Seat> listBookedSeatsInSchedule(Long showingScheduleId, Long movieId);
 
}
