package vn.movie.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import vn.movie.exception.file.FileStorageException;
import vn.movie.exception.file.MyFileNotFoundException;
import vn.movie.util.FileStorageProperties;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public String storeFile(String folderName, Long movieId, String movieName, MultipartFile file) {
        // Normalize file name
        String oldFileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        String extension = oldFileName.substring(oldFileName.lastIndexOf("."));
        String newFileName = StringUtils.cleanPath(movieName).toLowerCase().replaceAll(" ", "-");
        String newFileNameFull = newFileName + extension;

        try {
            // Copy file to the target location (Replacing existing file with the same name)
            String path = folderName + "/" + movieId;
            Path pathFolder = this.fileStorageLocation.resolve(path);

            // Create directory if not exits
            createDirectory(pathFolder);

            // New file name and create absolute file
            String renamedFileName = renameFile(pathFolder, newFileName, extension);
            Files.copy(file.getInputStream(), Paths.get(renamedFileName));
            return renamedFileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + newFileNameFull + ". Please try again!", ex);
        }
    }

    public void storeListFile(String folderName, Long movieId, String movieName, MultipartFile[] files) {
        for (MultipartFile file : files) {
            storeFile(folderName, movieId, movieName, file);
        }
    }

    // This method auto rename file if existed
    private String renameFile(Path directory, String fileName, String extension) {
        int index = 0;
        String fullPath = directory.toString() + "/" + fileName + extension;
        File file = new File(fullPath);
        while (file.exists()) {
            fullPath = directory.toString() + "/" + fileName + "-" + index + extension;
            index++;
            file = new File(fullPath);
        }
        return fullPath;
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }

    public boolean createDirectory(Path path) {
        //Creating a File object
        File file = new File(path.toString());
        //Creating the directory
        return file.mkdir();
    }
}