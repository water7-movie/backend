package vn.movie.service;

import vn.movie.entity.Auditorium;

import java.util.List;

public interface AuditoriumService {

    Auditorium findAuditoriumByDateAndMovieId(String date, Long movieId);

    List<Auditorium> listAll();
}
