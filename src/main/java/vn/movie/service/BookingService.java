package vn.movie.service;

import vn.movie.entity.Booking;
import vn.movie.model.request.BookingRequest;
import vn.movie.model.response.success.SeatResponse;

import java.text.ParseException;
import java.util.List;

public interface BookingService {

    List<Booking> saveBooking(BookingRequest bookingRequest) throws ParseException;

    SeatResponse getAllSeatsByMoviesAndDate(String showingDate, Long movieId);
}
