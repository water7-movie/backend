package vn.movie.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.movie.entity.Movie;
import vn.movie.model.request.MovieRequest;

import java.util.List;

public interface MovieService {

    Page<Movie> listAllShowingMoviesWithPagination(Pageable pageable);

    Page<Movie> listAllComingMoviesWithPagination(Pageable pageable);

    Page<Movie> getMovieList(Pageable pageable);

    Movie createMovie(MovieRequest movieRequest);

    Movie getMovieById(Long id);

    int setStatusForMovies(Long id, int status);

    List<Movie> getListMovieByDate(String date);

    Movie editMovie(MovieRequest movieRequest, Long id);
}
