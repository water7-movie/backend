package vn.movie.service;

import vn.movie.entity.ShowingSchedule;

import java.util.List;

public interface ShowingScheduleService {

    List<ShowingSchedule> listMoviesByDate(String date, Long movieId);

}
