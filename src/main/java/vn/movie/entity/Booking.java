package vn.movie.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "Bookings")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(style = "yyyy-MM-dd HH:mm")
    private Date bookingDate;

    private double grandTotal;

    @Column(columnDefinition = "SMALLINT default 0")
    private int status; // -1: canceled, 0: pending, 1: confirmed

    @OneToOne(fetch = FetchType.LAZY)
    private Movie movie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Seat seat;

    @OneToOne(fetch = FetchType.LAZY)
    private ShowingSchedule showingSchedule;

    public Booking() {
    }

    public Booking(Date bookingDate, double grandTotal, int status, Movie movie,
                   User user, Seat seat, ShowingSchedule showingSchedule) {
        this.bookingDate = bookingDate;
        this.grandTotal = grandTotal;
        this.status = status;
        this.movie = movie;
        this.user = user;
        this.seat = seat;
        this.showingSchedule = showingSchedule;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public ShowingSchedule getShowingSchedule() {
        return showingSchedule;
    }

    public void setShowingSchedule(ShowingSchedule showingSchedule) {
        this.showingSchedule = showingSchedule;
    }
}
