package vn.movie.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "Seats")
public class Seat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
//    @JsonIgnore
    private Long id;

    @Column(columnDefinition = "CHAR", length = 2, nullable = false)
    private String row; // We can store by A, B, C, D, E, F,...

    @Column(columnDefinition = "SMALLINT", nullable = false)
    private int number;

    @Column(columnDefinition = "CHAR", length = 1, nullable = false)
    private String seatType; // Rules: "n": normal, "v": vip

    @ManyToOne
    @JsonIgnore
    private Auditorium auditorium;

    @OneToMany(mappedBy = "seat")
    private Set<Booking> bookingSet;

    public Seat() {
    }

    public Long getId() {
        return id;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getSeatType() {
        return seatType;
    }

    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }

    public Auditorium getAuditorium() {
        return auditorium;
    }

    public void setAuditorium(Auditorium auditorium) {
        this.auditorium = auditorium;
    }
}
