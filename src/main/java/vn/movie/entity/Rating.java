package vn.movie.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity(name="Ratings")
public class Rating {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Movie movie;

    private double rating;

    public Long getId() {
        return id;
    }
}