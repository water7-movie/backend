package vn.movie.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity(name="Movies")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String director;

    private String actors;

    @Column(nullable = false)
    private String genre;

    @Column(columnDefinition = "DATE", nullable = false)
    private Date releaseDate;

    @Column(nullable = false)
    private int duration; // stored by minutes

    @Column(columnDefinition = "TEXT")
    private String description;

    @Transient
    @JsonIgnore
    private MultipartFile imageFile;

    @Column(columnDefinition = "SMALLINT default 1")
    private int status; // 1: active, 0: inactive

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Rating> ratings;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<ShowingSchedule> showingScheduleSet;

    public Movie() {
    }

    public Movie(Long id, String title, String director, String actors, String genre, Date releaseDate, int duration, String description, int status) {
        this.id = id;
        this.title = title;
        this.director = director;
        this.actors = actors;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.duration = duration;
        this.description = description;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public MultipartFile getImageFile() {
        return imageFile;
    }

    public void setImageFile(MultipartFile imageFile) {
        this.imageFile = imageFile;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Set<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(Set<Rating> ratings) {
        this.ratings = ratings;
    }

    public Set<ShowingSchedule> getShowingScheduleSet() {
        return showingScheduleSet;
    }

    public void setShowingScheduleSet(Set<ShowingSchedule> showingScheduleSet) {
        this.showingScheduleSet = showingScheduleSet;
    }


}
