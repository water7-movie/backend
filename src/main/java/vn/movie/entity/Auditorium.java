package vn.movie.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "Auditoriums")
public class Auditorium {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @Column(nullable = false)
    private String name;

    @Transient
    private int seatsNumber; // Total of seats in a auditorium.

    @Column(columnDefinition = "SMALLINT default 1", nullable = false)
    private byte status; // 1: active, 0: inactive

    @OneToMany(mappedBy = "auditorium", fetch = FetchType.EAGER)
    private Set<Seat> seats;

    @OneToMany(mappedBy = "auditorium")
    @JsonIgnore
    private Set<ShowingSchedule> showingScheduleSet;

    public Auditorium() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeatsNumber() {
        return seats.size();
    }

    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

    public Set<Seat> getSeats() {
        return seats;
    }

    public void setSeats(Set<Seat> seats) {
        this.seats = seats;
    }

    public Set<ShowingSchedule> getShowingScheduleSet() {
        return showingScheduleSet;
    }

    public void setShowingScheduleSet(Set<ShowingSchedule> showingScheduleSet) {
        this.showingScheduleSet = showingScheduleSet;
    }
}
