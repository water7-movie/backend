package vn.movie.entity;

public enum ERole {
    ROLE_CUSTOMER,
    ROLE_ADMIN,
    ROLE_MODERATOR
}
