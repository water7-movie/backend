package vn.movie.model.request;

public class LoginRequest extends AbstractUserRequest {
    public LoginRequest(String email, String password) {
        super(email, password);
    }
}
