package vn.movie.model.request;

import io.swagger.annotations.ApiModel;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@ApiModel(value = "Login Model Request")
public class JwtTokenRequest {
	@Email
	private String email;

	@Size(min = 2, max = 128)
	private String password;

	public JwtTokenRequest() {}

	public JwtTokenRequest(String email, String password) {
		this.setEmail(email);
		this.setPassword(password);
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}