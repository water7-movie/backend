package vn.movie.model.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;
import vn.movie.util.MessageUtil;

import javax.persistence.Transient;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel(value = "Movie Model Request")
public class MovieRequest {

    private Long id;

    @NotNull
    @Size(min = 2, max = 255, message = MessageUtil.VALIDATED_VARCHAR_MESSAGE)
    private String title;

    @NotNull
    @Size(min = 2, max = 255, message = MessageUtil.VALIDATED_VARCHAR_MESSAGE)
    private String director;

    @Size(min = 2, max = 255, message = MessageUtil.VALIDATED_VARCHAR_MESSAGE)
    private String actors;

    @NotNull
    @Size(min = 2, max = 255, message = MessageUtil.VALIDATED_VARCHAR_MESSAGE)
    private String genre;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Future(message = MessageUtil.VALIDATED_DATE_MESSAGE)
    private Date releaseDate;

    @NotNull
    @Positive(message = MessageUtil.VALIDATED_NUMBER_MESSAGE)
    private int duration;

    @NotNull
    @Size(min = 2, max = 255, message = MessageUtil.VALIDATED_TEXT_MESSAGE)
    private String description;

    @Transient
    private MultipartFile[] imageFile;

    private int status;

    public MovieRequest() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public String getDirector() {
        return director;
    }

    public String getActors() {
        return actors;
    }

    public String getGenre() {
        return genre;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public int getDuration() {
        return duration;
    }

    public String getDescription() {
        return description;
    }

    public int getStatus() {
        return status;
    }

    public MultipartFile[] getImageFile() {
        return imageFile;
    }

    public void setImageFile(MultipartFile[] imageFile) {
        this.imageFile = imageFile;
    }
}
