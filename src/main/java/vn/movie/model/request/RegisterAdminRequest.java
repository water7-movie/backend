package vn.movie.model.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

public class RegisterAdminRequest extends AbstractUserRequest {

    public RegisterAdminRequest() {
        super();
    }

    public RegisterAdminRequest(String email, String password, String fullName,
                                String phone, String address, Set<String> roles) {
        super(email, password, fullName, phone, address, roles);
//        roles = new HashSet<>();
//        roles.add("ROLE_ADMIN");
    }

    public RegisterAdminRequest(String email, String password) {
        super(email, password);
        this.roles = new HashSet<>();
        this.roles.add("ROLE_ADMIN");
    }

    public RegisterAdminRequest(@NotNull @Email String email, @NotNull @Size(min = 6, max = 128) String password, Set<String> roles) {
        super(email, password, roles);
    }
}
