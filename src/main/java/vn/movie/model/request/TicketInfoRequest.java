package vn.movie.model.request;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class TicketInfoRequest {
    private String movieName;
    private int movieDuration;

    @DateTimeFormat(style = "yyyy-MM-dd HH:mm")
    private Date showingTime;
    private double ticketPrice;
    private String auditoriumName;

    public TicketInfoRequest() {
    }

    public TicketInfoRequest(String movieName, int movieDuration, Date showingTime, double ticketPrice, String auditoriumName) {
        this.movieName = movieName;
        this.movieDuration = movieDuration;
        this.showingTime = showingTime;
        this.ticketPrice = ticketPrice;
        this.auditoriumName = auditoriumName;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public int getMovieDuration() {
        return movieDuration;
    }

    public void setMovieDuration(int movieDuration) {
        this.movieDuration = movieDuration;
    }

    public Date getShowingTime() {
        return showingTime;
    }

    public void setShowingTime(Date showingTime) {
        this.showingTime = showingTime;
    }

    public double getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(double ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public String getAuditoriumName() {
        return auditoriumName;
    }

    public void setAuditoriumName(String auditoriumName) {
        this.auditoriumName = auditoriumName;
    }
}
