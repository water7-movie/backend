package vn.movie.model.request;

import java.util.HashSet;

public class RegisterClientRequest extends AbstractUserRequest {

    public RegisterClientRequest(String email, String password) {
        super(email, password);
        this.roles = new HashSet<>();
        this.roles.add("ROLE_CUSTOMER");
    }

    @Override
    public String toString() {
        return "RegisterClientRequest{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", fullName='" + fullName + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", roles=" + roles +
                ", status=" + status +
                '}';
    }
}
