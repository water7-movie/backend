package vn.movie.model.request;

import io.swagger.annotations.ApiModel;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@ApiModel(value = "User Model Request")
public class UserRequest {

    @NotNull
    @Email
    private String email;

    @NotNull
    @Size(min = 6, max = 128)
    private String password;

    private String fullName;
    private String phone;
    private String address;

    private Set<String> roles;

    private int status = 1;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
