package vn.movie.model.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

public abstract class AbstractUserRequest {
    @NotNull
    @Email
    protected String email;

    @NotNull
    @Size(min = 6, max = 128)
    protected String password;

    protected String fullName;
    protected String phone;
    protected String address;
    protected String gender;
    protected Date dateOfBirth;

    protected Set<String> roles;
    protected int status = 1;

    public AbstractUserRequest() {
    }

    public AbstractUserRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public AbstractUserRequest(String email, String password, String fullName,
                               String phone, String address) {
        this.email = email;
        this.password = password;
        this.fullName = fullName;
        this.phone = phone;
        this.address = address;
    }

    public AbstractUserRequest(String email, String password, String fullName,
                               String phone, String address, Set<String> roles) {
        this.email = email;
        this.password = password;
        this.fullName = fullName;
        this.phone = phone;
        this.address = address;
        this.roles = roles;
    }

    public AbstractUserRequest(@NotNull @Email String email, @NotNull @Size(min = 6, max = 128) String password, Set<String> roles) {
        this.email = email;
        this.password = password;
        this.roles = roles;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public int getStatus() {
        return status;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public void setRoles(Set<String> roles) {
        this.roles = roles;
    }

    public String getGender() {
        return gender;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }
}
