package vn.movie.model.request;

import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel(value = "Booking Model Request")
public class BookingRequest {

    private String bookingDate;

    private double grandTotal;

    private int status;

    private Long movieId;
    private Long customerId;
    private List<Long> seatIds;
    private Long showingScheduleId;

    public BookingRequest() {
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public List<Long> getSeatIds() {
        return seatIds;
    }

    public void setSeatIds(List<Long> seatIds) {
        this.seatIds = seatIds;
    }

    public Long getShowingScheduleId() {
        return showingScheduleId;
    }

    public void setShowingScheduleId(Long showingScheduleId) {
        this.showingScheduleId = showingScheduleId;
    }
}
