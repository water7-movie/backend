package vn.movie.model.response.success;

public class JwtTokenResponse<T> {
	private final String token;
	private T userInfo;

	public JwtTokenResponse(String token, T userInfo) {
		this.token = token;
		this.userInfo = userInfo;
	}

	public String getToken() {
		return this.token;
	}

	public T getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(T userInfo) {
		this.userInfo = userInfo;
	}
}