package vn.movie.model.response.success;

public abstract class AbstractResultResponse<T> {
    protected int code;
    protected T data;

    public AbstractResultResponse(int code, T data) {
        this.code = code;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
