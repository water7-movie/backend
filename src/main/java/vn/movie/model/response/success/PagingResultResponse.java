package vn.movie.model.response.success;

public class PagingResultResponse<T> extends AbstractResultResponse<T> {
    private PageInfo pageInfo;
    public PagingResultResponse(int errorCode, T data, PageInfo pageInfo) {
        super(errorCode, data);
        this.pageInfo = pageInfo;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }
}

