package vn.movie.model.response.success;

import vn.movie.model.request.AbstractUserRequest;

import java.util.Set;

public class UserResponse extends AbstractUserRequest {
    public UserResponse(String email, String password, String fullName, String phone, String address, int status) {
        super(email, password, fullName, phone, address);
        this.status = status;
    }
}
