package vn.movie.model.response.success;

import vn.movie.entity.Seat;
import vn.movie.model.request.TicketInfoRequest;

import java.util.List;

public class SeatResponse {
  private List<Seat> seats;
  private Long showingScheduleId;
  private TicketInfoRequest ticketInfo;

  public SeatResponse() {
  }

  public SeatResponse(List<Seat> seats, Long showingScheduleId, TicketInfoRequest ticketInfo) {
    this.seats = seats;
    this.showingScheduleId = showingScheduleId;
    this.ticketInfo = ticketInfo;
  }

  public List<Seat> getSeats() {
    return seats;
  }

  public void setSeats(List<Seat> seats) {
    this.seats = seats;
  }

  public Long getShowingScheduleId() {
    return showingScheduleId;
  }

  public void setShowingScheduleId(Long showingScheduleId) {
    this.showingScheduleId = showingScheduleId;
  }

  public TicketInfoRequest getTicketInfo() {
    return ticketInfo;
  }

  public void setTicketInfo(TicketInfoRequest ticketInfo) {
    this.ticketInfo = ticketInfo;
  }
}
