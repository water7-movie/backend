package vn.movie.model.response.error;

import io.swagger.annotations.ApiModel;

@ApiModel(value="Api Error Response")
public class ApiErrorResponse extends AbstractErrorResponse {
    private long timeStamp;
    private String error;
    private Object errors;

    public ApiErrorResponse(int errorCode, String message, long timeStamp, String error, Object errors) {
        super(errorCode, message);
        this.timeStamp = timeStamp;
        this.error = error;
        this.errors = errors;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Object getErrors() {
        return errors;
    }

    public void setErrors(Object errors) {
        this.errors = errors;
    }
}
