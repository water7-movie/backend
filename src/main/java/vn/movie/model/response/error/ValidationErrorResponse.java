package vn.movie.model.response.error;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "Validation Error Response")
public class ValidationErrorResponse<T> extends AbstractErrorResponse {
    private T errors;

    public ValidationErrorResponse(int errorCode, String message, T errors) {
        super(errorCode, message);
        this.errors = errors;
    }

    public T getErrors() {
        return errors;
    }

    public void setErrors(T errors) {
        this.errors = errors;
    }
}
