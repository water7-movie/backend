package vn.movie.model.response.error;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "Only Message Response")
public class BaseErrorResponse extends AbstractErrorResponse {
    public BaseErrorResponse(int errorCode, String message) {
        super(errorCode, message);
    }
}
