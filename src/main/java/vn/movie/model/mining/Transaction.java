package vn.movie.model.mining;

public class Transaction {

    private Long customerId;
    private Long movieId;

    public Transaction(Long customerId, Long movieId) {
        this.customerId = customerId;
        this.movieId = movieId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }
}
