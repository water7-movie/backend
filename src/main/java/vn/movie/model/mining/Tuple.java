package vn.movie.model.mining;

import java.util.HashSet;
import java.util.Set;

public class Tuple {

    private Set<Long> itemset;
    private int support;

    public Tuple() {
        this.itemset = new HashSet<>();
        this.support = -1;
    }

    public Tuple(Set<Long> s) {
        this.itemset = s;
        this.support = -1;
    }

    public Tuple(Set<Long> s, int i) {
        this.itemset = s;
        this.support = i;
    }

    public Set<Long> getItemset() {
        return itemset;
    }

    public void setItemset(Set<Long> itemset) {
        this.itemset = itemset;
    }

    public int getSupport() {
        return support;
    }

    public void setSupport(int support) {
        this.support = support;
    }
}
