package vn.movie.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.movie.entity.Booking;
import vn.movie.entity.Seat;
import vn.movie.model.mining.Transaction;
import vn.movie.model.request.TicketInfoRequest;

import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Long> {
    @Query("select b.seat from Bookings b " +
            "where b.showingSchedule.id = :time " +
            "and b.movie.id = :movieId " +
            "order by b.seat.row ASC ")
    List<Seat> findBookedSeats(@Param("time") Long showingScheduleId, @Param("movieId") Long movieId);

    @Query("select new vn.movie.model.mining.Transaction(b.user.id, b.movie.id) from Bookings b")
    List<Transaction> listAllTransaction();

    @Query("select b from Bookings b where b.showingSchedule.id = ?1 and b.seat.id = ?2")
    Booking getBookingByShowingScheduleAndSeat(Long showingScheduleId, Long seatId);

  @Query("select s from Seats s"
          + " where s.auditorium.id in ("
          + " select s1.auditorium.id from ShowingSchedules s1"
          + " where function('to_char', s1.showingDate, 'yyyy-MM-dd HH24:MI:SS') like :time"
          + " and s1.movie.id = :movieId)")
  List<Seat> getAllSeatsByMoviesAndShowingDate(@Param("time") String showingScheduleDate,
    		@Param("movieId") Long movieId);

  @Query("select new vn.movie.model.request.TicketInfoRequest(m.title, m.duration, sh.showingDate, sh.price, a.name) from ShowingSchedules "
          + "sh join Auditoriums a on a.id = sh.auditorium.id join Movies m on m.id = sh.movie.id "
          + "where sh.id = :showingScheduleId"
  )
    TicketInfoRequest getTicketInfo(Long showingScheduleId);
}
