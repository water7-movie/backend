package vn.movie.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.movie.entity.Auditorium;

public interface AuditoriumRepository extends JpaRepository<Auditorium, Long> {

}
