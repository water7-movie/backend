package vn.movie.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.movie.entity.Movie;

import javax.transaction.Transactional;

public interface MovieRepository extends JpaRepository<Movie, Long> {

    Movie findByTitle(String title);

    @Query(value = "SELECT m FROM Movies m " +
            "WHERE m.releaseDate <= CURRENT_DATE AND m.status = 1")
    Page<Movie> findShowingMoviesWithQuery(Pageable pageable);

    @Query(value = "SELECT m FROM Movies m " +
            "WHERE m.releaseDate > CURRENT_DATE AND m.status = 1")
    Page<Movie> findComingMoviesWithQuery(Pageable pageable);

    @Query(value = "SELECT m FROM Movies m " +
            "WHERE  m.status <> -1")
    Page<Movie> findMovieNDelete(Pageable pageable);

    @Transactional
    @Modifying
    @Query("update Movies m set m.status = :status where m.id = :id")
    int setStatusForMovie(@Param("status") int status, @Param("id") Long id);

}
