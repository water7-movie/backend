package vn.movie.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import vn.movie.entity.Auditorium;
import vn.movie.entity.Movie;
import vn.movie.entity.ShowingSchedule;

import java.util.List;

public interface ShowingScheduleRepository extends JpaRepository<ShowingSchedule, Long> {

    @Query("select s from ShowingSchedules s " +
            "where function('to_char', s.showingDate, 'yyyy-MM-dd') like :date " +
            "and s.movie.id = :movieId")
    List<ShowingSchedule> findByDate(@Param("date") String date, @Param("movieId") Long movieId);

    @Query("select distinct s.auditorium from ShowingSchedules s " +
            "where function('to_char', s.showingDate, 'yyyy-MM-dd HH24:MI') like :date " +
            "and s.movie.id = :movieId")
    Auditorium findAuditorium(@Param("date") String date, @Param("movieId") Long movieId);

    @Query("select distinct s.movie from ShowingSchedules s " +
            "where function('to_char', s.showingDate, 'yyyy-MM-dd') like :date")
    List<Movie> findMoviesByDate(@Param("date") String date);

    @Query("select s1 from ShowingSchedules s1"
            + " where function('to_char', s1.showingDate, 'yyyy-MM-dd HH24:MI:SS') like :time"
            + " and s1.movie.id = :movieId")
    ShowingSchedule findShowingSchedulesByDateAndMovie(@Param("time") String showingScheduleDate,
                                                       @Param("movieId") Long movieId);
}
