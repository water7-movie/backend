package vn.movie.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.movie.entity.ERole;
import vn.movie.entity.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(ERole name);
}