package vn.movie.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import vn.movie.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
    @Query(value = "SELECT u FROM Users u WHERE u.email = ?1 and u.status = ?2")
    Optional<User> findByEmailAndStatus(String email, int status);
    Boolean existsByEmail(String email);
}
