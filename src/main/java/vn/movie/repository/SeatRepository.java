package vn.movie.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.movie.entity.Seat;

public interface SeatRepository extends JpaRepository<Seat, Long> {
}
