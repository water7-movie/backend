package vn.movie.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import vn.movie.entity.Movie;
import vn.movie.exception.movie.CustomMethodArgumentNotValidException;
import vn.movie.model.request.MovieRequest;
import vn.movie.model.response.success.AbstractResultResponse;
import vn.movie.model.response.success.BaseResultResponse;
import vn.movie.model.response.success.PageInfo;
import vn.movie.model.response.success.PagingResultResponse;
import vn.movie.service.MovieService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("admin/movies")
@Api(value = "Movie Admin APIs")
@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
public class MovieAdminController {
    private final MovieService movieService;

    public MovieAdminController(MovieService movieService) {
        this.movieService = movieService;
    }

    @PostMapping
    @ApiOperation(value = "Tạo phim", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<Map<String, Long>> createMovie(@Valid @ModelAttribute MovieRequest movieRequest,
                                                                 BindingResult bindingResult
    ) throws CustomMethodArgumentNotValidException {
        if (bindingResult.hasErrors()) {
            throw new CustomMethodArgumentNotValidException(bindingResult);
        } else {
            Movie movie = movieService.createMovie(movieRequest);
            return getResponse(movie);
        }
    }

    @PutMapping("{id}")
    @ApiOperation(value = "Cập nhật phim", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<Map<String, Long>> editMovie(@Valid @ModelAttribute MovieRequest movieRequest,
                                                               BindingResult bindingResult,
                                                               @PathVariable Long id
    ) throws CustomMethodArgumentNotValidException {
        if (bindingResult.hasErrors()) {
            throw new CustomMethodArgumentNotValidException(bindingResult);
        } else {
            Movie movie = movieService.editMovie(movieRequest, id);
            return getResponse(movie);
        }
    }

    @GetMapping
    @ApiOperation(value = "Lấy danh sách phim(có phân trang)", response = PagingResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<List<Movie>> getMovieList(@RequestParam(name = "page", required = false, defaultValue = "0") int page,
                                                            @RequestParam(name = "size", required = false, defaultValue = "12") int size,
                                                            @RequestParam(name = "direction", required = false, defaultValue = "descend") String direction,
                                                            @RequestParam(name = "column", required = false, defaultValue = "id") String column
    ) {
        Pageable pageable;
        if (direction.equals("descend")) {
            pageable = PageRequest.of(page, size, Sort.Direction.DESC, column);
        } else {
            pageable = PageRequest.of(page, size, Sort.Direction.ASC, column);
        }

        Page<Movie> content = movieService.getMovieList(pageable);

        return new PagingResultResponse<>(HttpStatus.OK.value(), content.getContent(), new PageInfo(page, size,
                (int) content.getTotalElements(),
                content.getTotalPages()));
    }

    @GetMapping("/setStatusForMovie")
    @ApiOperation(value = "Cập nhật trạng thái phim", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<Integer> setStatusForMovies(@RequestParam(name = "id") Long id,
                                                              @RequestParam(name = "status", required = false, defaultValue = "-1") int status) {
        int movieId = movieService.setStatusForMovies(id, status);
        return new BaseResultResponse<>(HttpStatus.OK.value(), movieId);
    }

    private AbstractResultResponse<Map<String, Long>> getResponse(Movie movie) {
        Map<String, Long> data = new HashMap<>();
        data.put("id", movie.getId());
        return new BaseResultResponse<>(HttpStatus.OK.value(), data);
    }
}


