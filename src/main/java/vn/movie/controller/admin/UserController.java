package vn.movie.controller.admin;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.movie.entity.User;
import vn.movie.exception.movie.CustomMethodArgumentNotValidException;
import vn.movie.model.request.RegisterAdminRequest;
import vn.movie.model.response.success.AbstractResultResponse;
import vn.movie.model.response.success.BaseResultResponse;
import vn.movie.service.UserService;

import javax.validation.Valid;


@RestController
@RequestMapping("admin/users")
@Api(value = "User APIs")
@PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR')")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    @ApiOperation(value = "Tạo User admin", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<User> createUser(@Valid @RequestBody RegisterAdminRequest userRequest,
                                                   BindingResult bindingResult) throws CustomMethodArgumentNotValidException {
        if (bindingResult.hasErrors()) {
            throw new CustomMethodArgumentNotValidException(bindingResult);
        } else {
            User createdUser = userService.createUser(userRequest);
            return new BaseResultResponse<>(HttpStatus.OK.value(), createdUser);
        }
    }
}
