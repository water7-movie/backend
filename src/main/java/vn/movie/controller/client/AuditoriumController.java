package vn.movie.controller.client;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vn.movie.entity.Auditorium;
import vn.movie.entity.Seat;
import vn.movie.model.response.success.AbstractResultResponse;
import vn.movie.model.response.success.BaseResultResponse;
import vn.movie.service.AuditoriumService;
import vn.movie.service.SeatService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("client/auditoriums")
@CrossOrigin("*")
@Api(value = "Auditorium APIs")
@PreAuthorize("hasRole('CUSTOMER') or hasRole('ADMIN') or hasRole('MODERATOR')")
public class AuditoriumController {

    private final AuditoriumService auditoriumService;
    private final SeatService seatService;

    public AuditoriumController(AuditoriumService auditoriumService,
                                SeatService seatService) {
        this.auditoriumService = auditoriumService;
        this.seatService = seatService;
    }

    @GetMapping
    @ApiOperation(value = "Lấy danh sách tất rạp chiếu phim", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<List<Auditorium>> getAll() {
        return new BaseResultResponse<>(HttpStatus.OK.value(), auditoriumService.listAll());
    }

    @GetMapping("seats")
    @ApiOperation(value = "Lấy danh sách tất cả ghế đã đặt trong 1 xuất chiếu nào đó", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<List<Seat>> listBookedSeatsInSchedule(@RequestParam(name = "movieId") Long movieId,
                                                                @RequestParam(name = "showingScheduleId") Long showingScheduleId) {

        return new BaseResultResponse<>(HttpStatus.OK.value(), seatService.listBookedSeatsInSchedule(showingScheduleId, movieId));
    }

    @GetMapping("{movieId}")
    @ApiOperation(value = "Lấy thông tin rạp chiếu phim bằng ID", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<Auditorium> getAuditoriumById(@PathVariable("movieId") Long movieId,
                                                        @RequestParam(value = "date", defaultValue = "") @DateTimeFormat(pattern = "yyyy-MM-dd HH24:mi") String date,
                                                        @RequestParam("timeShowing") String timeShowing) {
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        Date currentDate = new Date();
        String dateQuery = date + " " + timeShowing;
        if (date.equalsIgnoreCase("")) dateQuery = simpleDateFormat.format(currentDate);
        Auditorium result = auditoriumService.findAuditoriumByDateAndMovieId(dateQuery, movieId);
        return new BaseResultResponse<>(HttpStatus.OK.value(), result);
    }
}
