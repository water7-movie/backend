package vn.movie.controller.client;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vn.movie.entity.ShowingSchedule;
import vn.movie.model.response.success.AbstractResultResponse;
import vn.movie.model.response.success.BaseResultResponse;
import vn.movie.service.MovieService;
import vn.movie.service.ShowingScheduleService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("home")
@CrossOrigin("*")
@Api(value = "ShowingSchedule APIs")
public class ShowingScheduleController {

    private final ShowingScheduleService showingScheduleService;
    private final MovieService movieService;

    public ShowingScheduleController(ShowingScheduleService showingScheduleService,
                                     MovieService movieService) {
        this.showingScheduleService = showingScheduleService;
        this.movieService = movieService;
    }

    @GetMapping(value = "movies/schedules")
    @ApiOperation(value = "Lấy danh sách lịch chiếu của 1 bộ phim trong 1 ngày nhất định", response = AbstractResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<Object> listTheMovieScheduleInDayByDate(@RequestParam(name = "type", required = false) String type,
                                                                          @RequestParam(name = "movieId", required = false) Long movieId,
                                                                          @RequestParam(value = "date", defaultValue = "") @DateTimeFormat(pattern = "yyyy-MM-dd") String date) {
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date();
        String dateQuery = date;
        if (type.equalsIgnoreCase("0")) {
            List<ShowingSchedule> result;
            if (date.equalsIgnoreCase("")) dateQuery = simpleDateFormat.format(currentDate);

            result = showingScheduleService.listMoviesByDate(dateQuery, movieId);

            return new BaseResultResponse<>(HttpStatus.OK.value(), result);
        } else {
            return new BaseResultResponse<>(HttpStatus.OK.value(), movieService.getListMovieByDate(date));
        }


    }
}
