package vn.movie.controller.client;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.movie.entity.User;
import vn.movie.exception.movie.CustomMethodArgumentNotValidException;
import vn.movie.model.request.RegisterClientRequest;
import vn.movie.model.response.success.AbstractResultResponse;
import vn.movie.model.response.success.BaseResultResponse;
import vn.movie.model.response.success.UserResponse;
import vn.movie.service.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("api/users")
public class CustomerController {
    private final UserService userService;

    public CustomerController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public AbstractResultResponse<User>
    createUserAndCustomer(@Valid @RequestBody RegisterClientRequest userReq, BindingResult bindingResult)
            throws CustomMethodArgumentNotValidException {
        System.out.println(userReq.toString());
        if (bindingResult.hasErrors()) {
            throw new CustomMethodArgumentNotValidException(bindingResult);
        } else {
            User createdUser = userService.createUser(userReq);
            return new BaseResultResponse<>(HttpStatus.OK.value(), createdUser);
        }
    }
}
