package vn.movie.controller.client;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import vn.movie.entity.Movie;
import vn.movie.model.response.success.AbstractResultResponse;
import vn.movie.model.response.success.BaseResultResponse;
import vn.movie.model.response.success.PageInfo;
import vn.movie.model.response.success.PagingResultResponse;
import vn.movie.service.MovieService;
import vn.movie.service.RecommendService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("home")
@CrossOrigin(origins = "*")
@Api(value = "Movie APIs")
public class MovieController {

    private final MovieService movieService;

    private final RecommendService recommendService;

    public MovieController(MovieService movieService,
                           RecommendService recommendService) {
        this.movieService = movieService;
        this.recommendService = recommendService;
    }

    @GetMapping("movies")
    @ApiOperation(value = "Lấy danh sách tất cả phim(có phân trang)", response = PagingResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<List<Movie>> listAllMovies(@RequestParam(name = "page", required = false, defaultValue = "0") int page,
                                                             @RequestParam(name = "size", required = false, defaultValue = "12") int size,
                                                             @RequestParam(name = "query", required = false,
                                                                     defaultValue = "now-showing") String query) {
        Page<Movie> resultPage;
        Pageable pageable = PageRequest.of(page, size, Sort.Direction.ASC, "id");

        if ("coming-soon".equals(query)) {
            resultPage = movieService.listAllComingMoviesWithPagination(pageable);
        } else {
            resultPage = movieService.listAllShowingMoviesWithPagination(pageable);
        }

        return new PagingResultResponse<>(
                HttpStatus.OK.value(),
                resultPage.getContent(),
                new PageInfo(page, size,
                (int) resultPage.getTotalElements(),
                resultPage.getTotalPages())
        );
    }

    @GetMapping("movies/{id}")
    @ApiOperation(value = "Lấy thông tin phim bằng ID", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<Movie> getMovieById(@PathVariable Long id) {
        return new BaseResultResponse<>(HttpStatus.OK.value(), movieService.getMovieById(id));
    }

    @GetMapping("movies/recommendation/{movieId}")
    @ApiOperation(value = "Lấy danh sách phim được recommend (Data Mining)", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<List<Movie>> recommendMovie(@PathVariable Long movieId) {
        Set<Long> result = recommendService.recommendMovie(movieId);
        List<Movie> recommendedMovieList = new ArrayList<>();

        for(Long i : result) {
            recommendedMovieList.add(movieService.getMovieById(i));
        }

        return new BaseResultResponse<>(HttpStatus.OK.value(), recommendedMovieList);
    }
}
