package vn.movie.controller.client;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import vn.movie.entity.Booking;
import vn.movie.model.request.BookingRequest;
import vn.movie.model.response.success.AbstractResultResponse;
import vn.movie.model.response.success.BaseResultResponse;
import vn.movie.model.response.success.SeatResponse;
import vn.movie.service.BookingService;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("client/bookings")
@CrossOrigin("*")
@Api(value = "Booking APIs")
@PreAuthorize("hasRole('CUSTOMER') or hasRole('ADMIN') or hasRole('MODERATOR')")
public class BookingController {

    private final BookingService bookingService;

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @PostMapping
    @ApiOperation(value = "Lấy thông tin phim bằng ID", response = BaseResultResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Thành công"),
            @ApiResponse(code = 401, message = "Chưa xác thực"),
            @ApiResponse(code = 403, message = "Truy cập bị cấm"),
            @ApiResponse(code = 404, message = "Không tìm thấy api"),
            @ApiResponse(code = 500, message = "Không thành công")
    })
    public AbstractResultResponse<List<Booking>> bookingTicket(@RequestBody BookingRequest bookingRequest) throws ParseException {
        List<Booking> bookings = bookingService.saveBooking(bookingRequest);
        return new BaseResultResponse<>(HttpStatus.NO_CONTENT.value(), bookings);
    }

    @GetMapping
    public AbstractResultResponse<SeatResponse> getSeatsByMovieIdAndDate(
            @RequestParam(name = "showingDate") String showingDate,
            @RequestParam(name = "movieId") Long movieId) {
        SeatResponse seats = bookingService.getAllSeatsByMoviesAndDate(showingDate, movieId);
        return new BaseResultResponse<>(HttpStatus.OK.value(), seats);
    }
}
