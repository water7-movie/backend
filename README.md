**Hướng dẫn chạy project backend**

1. ###### Cài đặt:
- **IDE:** Spring Tool Suite hoặc Intellij
- **Database:** PostgresSQL
- **package:** npm

2. ###### Chạy project:
- Mở postgres và tạo database có tên là _MovieBookingDB_.
- Vào project và mở file có tên là _application.properties_ và sửa thông tin đăng nhập username và password postgres thành của mình.
Cụ thể: 
_spring.datasource.username='ten_username_postgresql'_
_spring.datasource.password='password'_

- Nhấn nút Run project.

3. Thêm dữ liệu để chạy thử:
- Tất cả dữ liệu ở trong thư mục data trong project này
- Mở postgresql lên và chạy **lần lượt** từng file: 

_MovieBookingDB_public_auditoriums.sql_
_MovieBookingDB_public_customers.sql_
_MovieBookingDB_public_movies.sql_
_MovieBookingDB_public_seats.sql_
_MovieBookingDB_public_users.sql_
_MovieBookingDB_public_showing_schedules.sql_
_MovieBookingDB_public_bookings.sql_


**Đã xong.**